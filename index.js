const stylus = require('stylus');
const path = require('path');
const fs = require('fs');
const Core = require('css-modules-loader-core');
const createFilter = require('rollup-pluginutils').createFilter;
const csso = require('csso');

const range = 'z'.charCodeAt(0) - 'a'.charCodeAt(0) + 1;
const totalRange = range * 2;
const range1 = 'a'.charCodeAt(0);
const range2 = 'A'.charCodeAt(0);

function toIdentifier(num) {
  let ident = '';
  do {
    let code = num % totalRange;
    let increment = code >= range ? range2 : range1;
    ident = String.fromCharCode((code % range) + increment) + ident;
    num -= code;
    num /= totalRange;
    num -= 1;
  } while (num >= 0);
  return ident;
}

function catResults(map) {
  return Object.values(map).join('\n\n')
}

function camelCase(str) {
  return str.replace(/-([a-z])/g, g => g[1].toUpperCase());
}

module.exports = function stylusCSSModules(options = {}) {
  const cssModules = new Core();
  const filter = createFilter(options.include, options.exclude);
  const resultMap = {};
  const scopes = [];
  let counter = 0;

  return {
    async transform(code, id) {
      if (!filter(id) || path.extname(id) !== '.styl') {
        return null;
      }

      const relativePath = path.relative(process.cwd(), id);

      const style = stylus(code);
      style.set('filename', relativePath);
      if (options.use) {
        style.use(options.use);
      }
      const css = await style.render();

      var {
        injectableSource,
        exportTokens
      } = await cssModules.load(css, relativePath, null);

      const currentScope = [];
      const newExportTokens = Object.keys(exportTokens).reduce((mem, key) => {
        let ident = (options.includeLocalName ? `${key}_` : '') + toIdentifier(counter++);
        mem[camelCase(key)] = ident;
        currentScope.push(ident);
        injectableSource = injectableSource.replace(new RegExp(exportTokens[key], 'g'), ident);
        return mem;
      }, {});
      scopes.push(currentScope);

      resultMap[relativePath] = injectableSource;

      return {
        id: `${id}.css`,
        code: `export default ${JSON.stringify(newExportTokens)}`,
        map: {mappings: ''}
      };
    },

    async onwrite({ dest }) {
      var css = catResults(resultMap);
      if (options.map) {
        css = await options.map(css);
      }
      if (options.compress) {
        css = csso.minify(catResults(resultMap), {
          restructure: true,
          usage: {
            scopes
          }
        }).css;
      }
      return await new Promise((res, rej) => fs.writeFile(
        path.join(path.dirname(dest), path.basename(dest, path.extname(dest)) + '.css'),
        css,
        'utf-8',
        (err) => err ? rej(err) : res()
      ));
    }

  }

}
